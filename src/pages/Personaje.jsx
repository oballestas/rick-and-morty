import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { ListaEpisodios } from '../components/Episodios/ListaEpisodios';
import image from '../assets/img/bannerImg.png';

export function Personaje() {
  let parametrosUrl = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);

  const URL = `https://rickandmortyapi.com/api/character/${parametrosUrl.personajeId}`;

  //Petición de los datos del personaje
  useEffect(() => {
    axios.get(URL).then((respuesta) => {
      setPersonaje(respuesta.data);
    });
  }, []);

  // Petición del los datos de los episodios de un personaje
  useEffect(() => {
    if (personaje) {
      let peticionesEpisodios = personaje.episode.map((episodio) => {
        return axios.get(episodio);
      });
      Promise.all(peticionesEpisodios).then((respuestas) => {
        setEpisodios(respuestas);
      });
    }
  }, [personaje]);

  function colorStatusPersonaje() {
    let colorStatus = '';
    if (personaje?.status.toLowerCase() === 'alive') {
      colorStatus = 'green';
    } else if (personaje?.status.toLowerCase() === 'dead') {
      colorStatus = 'red';
    } else {
      colorStatus = 'white';
    }
    return colorStatus;
  }

  let colorStatus = colorStatusPersonaje();

  return (
    <div className='logo' style={{ backgroundImage: `url(${image}` }}>
      {/* Información del personaje */}
      {personaje ? (
        <div>
          <h2 className='text-center  text-white pt-3'>{personaje.name}</h2>
          <div className='row'>
            <div className='col-1'></div>
            <div className='col-3 col-lg-3 col-md-4 col-sm-6 col-12'>
              <img
                src={personaje.image}
                className='card-img-top px-4 py-3'
                alt={personaje.name}
                style={{ maxWidth: 400 }}
              />
            </div>
            <div className='col-5 pt-3 text-white'>
              <p className='m-0'>
                - Status:
                <span style={{ color: colorStatus }}> {personaje.status}</span>
              </p>
              <p className='mb-0 text-white'>- Specie: {personaje.species}</p>
              <p className='mb-0 text-white'>
                - Origin: {personaje.origin.name}
              </p>
              <p className='mb-0 text-white'>
                -Last know location: {personaje.location.name}
              </p>
            </div>
            <div className='col-3'></div>
          </div>

          {/* Lista de Episodios */}

          <div>
            <div className='container'>
              <h3 className='px-5 text-center text-white'>Episodes</h3>
            </div>
            <div className='px-5 py-4'>
              {episodios ? (
                <ListaEpisodios episodios={episodios} />
              ) : (
                'Loading...'
              )}
            </div>
          </div>
        </div>
      ) : (
        'Loading...'
      )}
    </div>
  );
}
