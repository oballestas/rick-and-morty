import { Buscador } from '../components/Buscador/Buscador';
import { Banner } from '../components/Navbar/Banner';
import { ListadoPersonajes } from '../components/Personajes/ListadoPersonajes';
import { useEffect, useState } from 'react';
import image from '../assets/img/background.png';

export function Home() {
  let [busqueda, setBusqueda] = useState('');

  return (
    <div className=''>
      <Banner />
      <div className='logo' style={{ backgroundImage: `url(${image}` }}>
        <div className='container'>
          <h3 className='text-center  text-white pt-4 pb-3'>Characters</h3>
        </div>
        <Buscador valor={busqueda} onBuscar={setBusqueda} />
        <ListadoPersonajes busqueda={busqueda} />
      </div>
    </div>
  );
}
