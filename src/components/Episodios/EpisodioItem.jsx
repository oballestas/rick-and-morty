export function EpisodioItem({ name, air_date, episode }) {
  return (
    <div className='col-4 py-3 col-lg-3 col-md-4 col-sm-6 col-12'>
      <div className='card'>
        <div className='card-body text-center'>
          <h5 className='card-title'>{name}</h5>
          <p className='m-0'>Air date: {air_date}</p>
          <p>Episode Number: {episode}</p>
        </div>
      </div>
    </div>
  );
}
