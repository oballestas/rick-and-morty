import { EpisodioItem } from './EpisodioItem';

export function ListaEpisodios({ episodios }) {
  return (
    <div className='row'>
      {episodios.map((episodio) => {
        return <EpisodioItem key={episodio.data.id} {...episodio.data} />;
      })}
    </div>
  );
}
