export function Footer() {
  return (
    <div className='navbar-dark bg-dark'>
      <p className='text-white text-center py-2'>
        By: Óscar Ballestas - CAR IV 2022
      </p>
    </div>
  );
}
