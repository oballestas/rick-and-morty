import axios from 'axios';
import { useEffect, useState } from 'react';
import { PersonajeItem } from './PersonajeItem';

export function ListadoPersonajes({ busqueda }) {
  let [personajes, setPersonajes] = useState(null);
  let personajesFiltrados = personajes?.results;

  useEffect(() => {
    axios.get('https://rickandmortyapi.com/api/character').then((respuesta) => {
      setPersonajes(respuesta.data);
    });
  }, []);

  if (busqueda && personajes) {
    personajesFiltrados = personajes.results.filter((personaje) => {
      let nombrePersonajeMinuscula = personaje.name.toLowerCase();
      let buscardorMinuscula = busqueda.toLowerCase();
      return nombrePersonajeMinuscula.includes(buscardorMinuscula);
    });
  }

  return (
    <div className='row px-5 pb-3'>
      {personajesFiltrados
        ? personajesFiltrados.map((personaje) => {
            return <PersonajeItem {...personaje} key={personaje.id} />;
          })
        : 'Loading...'}
    </div>
  );
}
