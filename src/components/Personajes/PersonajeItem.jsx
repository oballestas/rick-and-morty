import { Link } from 'react-router-dom';

export function PersonajeItem({
  id,
  name,
  status,
  species,
  location,
  image,
  origin,
}) {
  function colorStatusPersonaje() {
    let colorStatus = '';
    if (status.toLowerCase() === 'alive') {
      colorStatus = 'green';
    } else if (status.toLowerCase() === 'dead') {
      colorStatus = 'red';
    } else {
      colorStatus = 'gray';
    }
    return colorStatus;
  }

  let colorStatus = colorStatusPersonaje();

  return (
    <div className='col-3 py-4 col-lg-3 col-md-4 col-sm-6 col-12'>
      <div className='card' style={{ maxWidth: '400px' }}>
        <img src={image} className='card-img-top' alt={name} />
        <div className='card-body text-center'>
          <Link
            className='nav-link'
            aria-current='page'
            to={`/personaje/${id}`}
          >
            <h5 className='card-title'>{name}</h5>
          </Link>
          <p className='m-0'>
            Status:
            <span style={{ color: colorStatus }}> {status}</span>
          </p>
          <p>Specie: {species}</p>
          <p className='mb-0'>Origin:</p>
          <p>{origin?.name}</p>
          <p className='mb-0'>Last know location:</p>
          <p>{location?.name}</p>
        </div>
      </div>
    </div>
  );
}
