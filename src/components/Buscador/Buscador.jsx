export function Buscador({ valor, onBuscar }) {
  return (
    <div className=''>
      <div className='row align-items-center'>
        <div className='col-7'></div>
        <div className='col-5 pb-3 px-5'>
          <input
            value={valor}
            type='text'
            className='form-control'
            aria-describedby='passwordHelpInline'
            placeholder='Search character...'
            onChange={(evento) => onBuscar(evento.target.value)}
          />
        </div>
      </div>
    </div>
  );
}
