import bannerImg from '../../assets/img/bannerImg.jpg';

export function Banner() {
  return (
    <div className='card bg-dark text-white'>
      <img
        src={bannerImg}
        className='card-img'
        alt='banner'
        style={{ height: '630px', width: '400' }}
      />
    </div>
  );
}
